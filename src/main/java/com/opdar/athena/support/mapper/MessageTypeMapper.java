package com.opdar.athena.support.mapper;

import com.opdar.athena.support.entities.MessageTypeEntity;
import com.opdar.plugins.mybatis.core.IBaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * Created by shiju on 2017/7/11.
 */
public interface MessageTypeMapper extends IBaseMapper<MessageTypeEntity> {
    Integer selectMaxCodeByAppId(@Param("appId") String appId);
}
