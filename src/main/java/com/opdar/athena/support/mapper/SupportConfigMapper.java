package com.opdar.athena.support.mapper;

import com.opdar.athena.support.entities.SupportConfigEntity;
import com.opdar.plugins.mybatis.core.IBaseMapper;

/**
 * Created by shiju on 2017/7/11.
 */
public interface SupportConfigMapper extends IBaseMapper<SupportConfigEntity> {
}
