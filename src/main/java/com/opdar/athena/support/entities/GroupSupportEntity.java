package com.opdar.athena.support.entities;

import com.opdar.athena.support.mapper.GroupMapper;
import com.opdar.athena.support.mapper.GroupSupportMapper;
import com.opdar.plugins.mybatis.annotations.Namespace;

import java.sql.Timestamp;

/**
 * Created by shiju on 2017/8/2.
 */
@Namespace(GroupSupportMapper.class)
public class GroupSupportEntity {
    private String id;
    private String groupId;
    private String supportId;
    private Integer vaild;
    private Timestamp createTime;
    private Timestamp updateTime;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getSupportId() {
        return supportId;
    }

    public void setSupportId(String supportId) {
        this.supportId = supportId;
    }

    public Integer getVaild() {
        return vaild;
    }

    public void setVaild(Integer vaild) {
        this.vaild = vaild;
    }

    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    public Timestamp getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Timestamp updateTime) {
        this.updateTime = updateTime;
    }
}
